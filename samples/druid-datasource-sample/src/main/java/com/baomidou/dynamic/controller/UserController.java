package com.baomidou.dynamic.controller;

import com.baomidou.dynamic.entity.User;
import com.baomidou.dynamic.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;

@RestController
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/user/add")
    public String addUser(@RequestParam String name, @RequestParam Integer age) {
        boolean result = userMapper.addUser(name, age);
        return result ? "新增用户成功" : "新增用户失败";
    }

    @RequestMapping("/user/update")
    public String updateUser(@RequestParam Integer id, @RequestParam String name, @RequestParam Integer age) {
        boolean result = userMapper.updateUser(id, name, age);
        return result ? "更新用户成功" : "更新用户失败";
    }

    @RequestMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id) {
        boolean result = userMapper.deleteUser(id);
        return result ? "删除用户成功" : "删除用户失败";
    }

    @RequestMapping("/users")
    @ResponseBody
    public List<User> users() {
        return userMapper.selectAll();
    }

}